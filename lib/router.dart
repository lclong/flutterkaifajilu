import 'package:flutter_demo_01/pages/detail.dart';
import 'package:flutter_demo_01/pages/list.dart';
import 'package:flutter_demo_01/pages/provideTest/myProvide/myProvide.dart';
import 'package:flutter_demo_01/pages/provideTest/myChangeNotifierProvider/myChangeNotifierProvider.dart';

class Router{
  static Map<String, dynamic> register(context) {
    var router = {
      "invitationList": (context) => InvitationRecordListPage(),
      "detail": (context) => Detail(),
      "myProvider": (context) => MyProvide(),
      "myChangeNotifierProvider": (context) => myChangeNotifierProvider()
    };
    return router;
  }
}