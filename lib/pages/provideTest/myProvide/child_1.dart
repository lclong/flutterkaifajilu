import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_demo_01/pages/provideTest/myProvide/child_1_1.dart';

class ProviderChild_1 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Text(
            "child_1：${Provider.of<String>(context)}"
          ),
          ProviderChild_1_1()
        ],
      ),
    );
  }
}