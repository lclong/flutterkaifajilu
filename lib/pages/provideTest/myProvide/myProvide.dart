import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_demo_01/pages/provideTest/myProvide/child_1.dart';

class MyProvide extends StatefulWidget {
  @override
  MyProvideState createState () => MyProvideState();
}
class MyProvideState extends State<MyProvide> {
  @override
  Widget build(BuildContext context) {
    return Provider<String>.value(
        value: "这是一个privide数据",
        child: Scaffold(
          appBar: AppBar(
            title: Text("Provider - Provider")
          ),
          body: ProviderChild_1()
        )
    );
  }
}