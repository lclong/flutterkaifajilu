import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProviderChild_1_1 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(30),
      child: Text(
        "child_1_1：${Provider.of<String>(context)}"
      ),
    );
  }
}