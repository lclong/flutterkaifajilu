import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_demo_01/pages/model/user.dart';
import 'package:flutter_demo_01/pages/provideTest/myChangeNotifierProvider/userModel.dart';

class Child_1 extends StatefulWidget {

  @override
  Child_1State createState() {
    return Child_1State();
  }

}

class Child_1State extends State<Child_1> {

  TextEditingController input_controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    UserModel userModel = Provider.of<UserModel>(context);
    return Container(
      color: Color(0xffeeeeee),
      height: 500,
      padding: EdgeInsets.all(30),
      child: Column(
        children: <Widget>[
          Text("用户姓名：${userModel?.user?.name}"),
          Text("用户年龄：${userModel?.user?.age}"),
          TextFormField(
            controller: input_controller,
            decoration: InputDecoration(border: OutlineInputBorder()),

          )
        ],
      ),
    );
  }




}