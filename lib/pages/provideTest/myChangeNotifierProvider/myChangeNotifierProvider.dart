import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_demo_01/pages/model/user.dart';
import 'package:flutter_demo_01/pages/provideTest/myChangeNotifierProvider/userModel.dart';
import 'package:flutter_demo_01/pages/provideTest/myChangeNotifierProvider/child_1.dart';

class myChangeNotifierProvider extends StatefulWidget {
  @override
  myChangeNotifierProviderState createState() {
    return myChangeNotifierProviderState();
  }
}

class myChangeNotifierProviderState extends State<myChangeNotifierProvider> {

  UserModel userModel = UserModel();

  @override
  void initState() {
    super.initState();

    User user = User.formJson({
      "name": "makr",
      "age": 45
    });

    userModel.increment(user);
  }


  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<UserModel>.value(
        value: userModel,
        child: Scaffold(
          appBar: AppBar(
            title: Text("myChangeNotifierProvider"),
          ),
          body: Container(
            child: Child_1(),
          ),
        ),
    );
  }
}