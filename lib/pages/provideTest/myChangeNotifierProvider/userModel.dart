import 'package:flutter/material.dart';
import 'package:flutter_demo_01/pages/model/user.dart';

class UserModel with ChangeNotifier {
  User user = User();

  User get value => user;// 把use暴露出来

  // 提供方法，用于更改数据
  void increment(User user2) {
    user = user2;
    notifyListeners(); // 通知所有听众，进行刷新
  }

}