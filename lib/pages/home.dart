import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'dart:async';
import 'package:flutter_demo_01/pages/components/homeItem.dart';

class MyHomePage extends StatefulWidget {
  String title = "flutter";
  MyHomePage({this.title});
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List list = [];

  void initState() {
    super.initState();
    setState(() {
      list = [
        {"pathName": "invitationList", "title": "邀请列表"},
        {"pathName": "myProvider", "title": "Provider - Provider"},
        {"pathName": "myChangeNotifierProvider", "title": "Provider - myChangeNotifierProvider"},
      ];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        color: Color.fromRGBO(255, 255, 255, 1),
        child: ListView.builder(
          itemCount: list.length,
          itemBuilder: (item, index) {
            var dataItem = list[index];
            return HomeItem(dataItem["pathName"], dataItem["title"]);
          },
        ),
      )
    );
  }




}
