import 'package:flutter/material.dart';

class User {
    String name = "";
    int age = 0;

    User({this.name, this.age});

    // 命名构造器
    User.formJson(Map<String, dynamic> json) {
      name = json['name'];
      age = json['age'];
    }
}