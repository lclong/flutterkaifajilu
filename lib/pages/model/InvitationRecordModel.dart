
class InvitationRecord {
  String title;
  Map<String, dynamic> content;

  InvitationRecord(this.title,this.content);

  // 命名构造器
  InvitationRecord.formJson(Map<String, dynamic> json) {
    title = json['title'];
    content = json['content'];
  }
}