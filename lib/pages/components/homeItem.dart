import 'package:flutter/material.dart';

class HomeItem extends StatefulWidget {
  String pathName;
  String title;

  HomeItem(this.pathName, this.title);

  @override
  HomeItemState createState() {
    return HomeItemState();
  }
}

class HomeItemState extends State<HomeItem> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, widget.pathName);
      },
      child: Container(
        width: 375,
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: Color(0xffdddddd),
                    width: 0.5
                )
            )
        ),
        child: Text(widget.title),
      ),
    );
  }
}