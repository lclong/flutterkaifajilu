import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'dart:async';

import 'package:flutter_demo_01/pages/model/InvitationRecordModel.dart';

class InvitationRecordListPage extends StatefulWidget {
  InvitationRecordListPage({Key key, this.title="邀请记录"}) : super(key: key);
  final String title;
  @override
  InvitationRecordListState createState() => InvitationRecordListState();
}

class InvitationRecordListState extends State<InvitationRecordListPage> {

  var dataList = [];

  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Container(
            color: Color.fromRGBO(246, 246, 246, 1),
            child: Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Offstage(
                  offstage: dataList.length > 0,
                  child: Center(
                    child: Text(
                      "数据正在加载...",
                      style: TextStyle(
                          color: Colors.blue,
                          fontSize: 23
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: ListView.builder(
                    itemCount: dataList.length,
                    itemBuilder: (item, index) {
                      var dataItem = dataList[index];
                      return Container(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          margin: EdgeInsets.symmetric(vertical: 5),
                          child: GestureDetector(
                            onTap: (){
                              Navigator.pushNamed(context, "detail");
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 10),
                                  child: Text(
                                    dataItem.title,
                                    style: TextStyle(
                                        color: Color.fromRGBO(147, 153, 148, 1),
                                        fontSize: 13
                                    ),
                                  ),
                                ),
                                Divider(height: .0),
                                Container(
                                  padding: EdgeInsets.symmetric(vertical: 15),
                                  child: Flex(
                                    direction: Axis.horizontal,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Image(
                                            image: NetworkImage(dataItem.content["image"]),
                                            height: 80,
                                            width: 80,
                                            fit: BoxFit.cover
                                        ),
                                      ),
                                      Expanded(
                                          flex: 1,
                                          child: Container(
                                            padding: EdgeInsets.only(left: 10),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  dataItem.content['itemName'],
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      color: Color.fromRGBO(48, 50, 49, 1),
                                                      fontSize: 18,
                                                      height: 1,
                                                      fontWeight: FontWeight.w500
                                                  ),
                                                ),
                                                Text(
                                                  dataItem.content['itemDesc'],
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      color: Color.fromRGBO(48, 50, 49, 1),
                                                      fontSize: 13,
                                                      height: 1.3
                                                  ),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(top: 10),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: <Widget>[
                                                      Offstage(
                                                        offstage: !dataItem.content["flowerCountShow"],
                                                        child: Row(
                                                          mainAxisSize: MainAxisSize.min,
                                                          children: <Widget>[
                                                            Image(
                                                                image: NetworkImage("https://static-cdn.xiangwushuo.com/miniprogram/static/common/flower.png"),
                                                                width: 18,
                                                                height: 18,
                                                                fit: BoxFit.contain
                                                            ),
                                                            Container(
                                                              margin: EdgeInsets.only(left: 5),
                                                              child: Text(
                                                                dataItem.content["flowerCount"],
                                                                style: TextStyle(
                                                                    color: Color.fromRGBO(230, 67, 64, 1),
                                                                    fontSize: 16
                                                                ),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      ClipRRect(
                                                        borderRadius: BorderRadius.circular(8),
                                                        child: Container(
                                                          color: Color.fromRGBO(255, 233, 171, 1),
                                                          padding: EdgeInsets.symmetric(horizontal: 10),
                                                          height: 20,
                                                          child: Row(
                                                            mainAxisSize: MainAxisSize.min,
                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            children: <Widget>[
                                                              Image(
                                                                  image: AssetImage("assets/images/eye.png"),
                                                                  width: 14,
                                                                  height: 14,
                                                                  fit: BoxFit.contain
                                                              ),
                                                              Container(
                                                                margin: EdgeInsets.only(left: 5),
                                                                child: Text(
                                                                  "${dataItem.content["lookNumber"]}人看过",
                                                                  style: TextStyle(
                                                                      color: Color.fromRGBO(48, 50, 49, 1),
                                                                      fontSize: 12
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          )
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                      );
                    },
                  ),
                )
              ],
            )
        )
    );
  }

  getData() async{
    await Future.delayed(const Duration(milliseconds: 2000)); // 延时2秒

    var list = [{
      "title": "已成功邀请242人看这个宝贝，瓜分到881点奖励",
      "content": {
        "image": "https://imgs-1253854453.image.myqcloud.com/a7518a44d3bb056a0f143edf69e2cc16.jpg",
        "itemName": "网红款玫红色包",
        "itemDesc": "送给有缘人，好好照顾多肉哦，新鲜多缘人，好好照顾多肉哦，新鲜多",
        "flowerCount": "123",
        "flowerCountShow":true,
        "lookNumber": "1221"
      }
    },{
      "title": "已成功邀请242人看这个宝贝，瓜分到881点奖励",
      "content": {
        "image": "https://imgs-1253854453.image.myqcloud.com/a7518a44d3bb056a0f143edf69e2cc16.jpg",
        "itemName": "网红款玫红色包",
        "itemDesc": "送给有缘人，好好照顾多肉哦，新鲜多缘人，好好照顾多肉哦，新鲜多",
        "flowerCount": "123",
        "flowerCountShow":false,
        "lookNumber": "1221"
      }
    },{
      "title": "已成功邀请242人看这个宝贝，瓜分到881点奖励",
      "content": {
        "image": "https://imgs-1253854453.image.myqcloud.com/a7518a44d3bb056a0f143edf69e2cc16.jpg",
        "itemName": "网红款玫红色包",
        "itemDesc": "送给有缘人，好好照顾多肉哦，新鲜多缘人，好好照顾多肉哦，新鲜多",
        "flowerCount": "123",
        "flowerCountShow":true,
        "lookNumber": "1221"
      }
    },{
      "title": "已成功邀请242人看这个宝贝，瓜分到881点奖励",
      "content": {
        "image": "https://imgs-1253854453.image.myqcloud.com/a7518a44d3bb056a0f143edf69e2cc16.jpg",
        "itemName": "网红款玫红色包",
        "itemDesc": "送给有缘人，好好照顾多肉哦，新鲜多缘人，好好照顾多肉哦，新鲜多",
        "flowerCount": "123",
        "flowerCountShow":true,
        "lookNumber": "1221"
      }
    },{
      "title": "已成功邀请242人看这个宝贝，瓜分到881点奖励",
      "content": {
        "image": "https://imgs-1253854453.image.myqcloud.com/a7518a44d3bb056a0f143edf69e2cc16.jpg",
        "itemName": "网红款玫红色包",
        "itemDesc": "送给有缘人，好好照顾多肉哦，新鲜多缘人，好好照顾多肉哦，新鲜多",
        "flowerCount": "123",
        "flowerCountShow":true,
        "lookNumber": "1221"
      }
    },{
      "title": "已成功邀请242人看这个宝贝，瓜分到881点奖励",
      "content": {
        "image": "https://imgs-1253854453.image.myqcloud.com/a7518a44d3bb056a0f143edf69e2cc16.jpg",
        "itemName": "网红款玫红色包",
        "itemDesc": "送给有缘人，好好照顾多肉哦，新鲜多缘人，好好照顾多肉哦，新鲜多",
        "flowerCount": "123",
        "flowerCountShow":true,
        "lookNumber": "1221"
      }
    }];

    List<InvitationRecord> recordList = List();
    for(var i = 0; i < list.length; i++) {
      InvitationRecord invitationRecord = InvitationRecord.formJson(list[i]);
      recordList.add(invitationRecord);
    }
    setState(() {
      dataList = recordList;
    });
  }

}
